
# Import required modules
import configparser
import argparse
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
from time import sleep

# read settings from config file
configParser = configparser.RawConfigParser()   
configFilePath = 'targetturner-exec.conf'
configParser.read(configFilePath)

NODE_ID = configParser.get('node-config', 'node_id')
MQTTSERVER = configParser.get('mqtt-config', 'server')
MQTTPORT = configParser.get('mqtt-config', 'port')
TOPIC = configParser.get('mqtt-config', 'topic')

# get arguments from command line
parser = argparse.ArgumentParser(description='Sensor Trigger Script')
parser.add_argument("--sensor1", default=1, help="The number of the node sensor 1 triggers")
parser.add_argument("--sensor2", default=1, help="The number of the node sensor 2 triggers")
parser.add_argument("--direction", default=0, help="The direction the targets turn, defaults to random, options are anti-clockwise =1, clockwise =2")
parser.add_argument("--duration", default=0, help="The duration the targets face for, defaults to random (2-10)")
parser.add_argument("--sound", default=1, help="should the buzzer be sounded? true/false")

args = parser.parse_args()
sensor1 = args.sensor1
sensor2 = args.sensor2
direction = args.direction
duration = args.duration
sound = args.sound

# Setup GPIO to meet our needs
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
RELAIS_1_GPIO = 17 # constant defining pin 17
RELAIS_2_GPIO = 23 # constant defining pin 23
RELAIS_3_GPIO = 24 # constant defining pin 24
GPIO.setup(RELAIS_1_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # GPIO Assign mode
GPIO.setup(RELAIS_2_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # GPIO Assign mode
GPIO.setup(RELAIS_3_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # GPIO Assign mode

print("Target Node ID: "+str(NODE_ID))
print("MQTT Server: "+str(MQTTSERVER))
print("MQQT Port: "+str(MQTTPORT))
print("MQTT Topic: "+str(TOPIC))

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connecting to topic: "+TOPIC)
    print("Connected with result code "+str(rc))
    print("Sensor Ready")

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(TOPIC)

# the callback for when we need to PUBLISH a message to the server
def callback1():
    print ('Sensor 1 Activation Detected')
    client.publish(TOPIC, "sensor1,direction,duration,sound")
    sleep(30)

def callback2():
    print ('Sensor 2 Activation Detected')
    client.publish(TOPIC, "sensor2,direction,duration,sound")
    sleep(30)

client = mqtt.Client()
client.on_connect = on_connect

client.connect(MQTTSERVER, int(MQTTPORT), 60)

GPIO.add_event_detect(RELAIS_1_GPIO, GPIO.RISING, callback=callback1, bouncetime=20000)
GPIO.add_event_detect(RELAIS_2_GPIO, GPIO.RISING, callback=callback2, bouncetime=20000)

try:
    GPIO.wait_for_edge(RELAIS_3_GPIO, GPIO.RISING) 
except KeyboardInterrupt:          # trap a CTRL+C keyboard interrupt
    print("Quitting, gleaning up GPIO...")  
    GPIO.cleanup()                 # resets all GPIO ports used by this program  


