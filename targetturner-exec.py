
# Import required modules
import configparser
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
from time import sleep

# read settings from config file
configParser = configparser.RawConfigParser()   
configFilePath = 'targetturner-exec.conf'
configParser.read(configFilePath)

NODE_ID = configParser.get('node-config', 'node_id')
MQTTSERVER = configParser.get('mqtt-config', 'server')
MQTTPORT = configParser.get('mqtt-config', 'port')
TOPIC = configParser.get('mqtt-config', 'topic')

# Setup GPIO to meet our needs
GPIO.setmode(GPIO.BCM) # GPIO Numbers instead of board numbers
RELAIS_1_GPIO = 4 # constant defining pin 4
RELAIS_2_GPIO = 5 # constant defining pin 5
RELAIS_3_GPIO = 6 # constant defining pin 6
RELAIS_4_GPIO = 13 # constant defining pin 13

GPIO.setup(RELAIS_1_GPIO, GPIO.OUT) # GPIO Assign mode
GPIO.setup(RELAIS_2_GPIO, GPIO.OUT) # GPIO Assign mode
GPIO.setup(RELAIS_3_GPIO, GPIO.OUT) # GPIO Assign mode
GPIO.setup(RELAIS_4_GPIO, GPIO.IN, pull_up_down=GPIO.PUD_DOWN) # GPIO Assign mode

print("Target Node ID: "+str(NODE_ID))
print("MQTT Server: "+str(MQTTSERVER))
print("MQQT Port: "+str(MQTTPORT))
print("MQTT Topic: "+str(TOPIC))

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connecting to topic: "+TOPIC)
    print("Connected with result code "+str(rc))
    print("Target Ready")

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(TOPIC)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))
    
    #payload example "b'1,1,5,1'"
    # identify message payload first digit=node, second digit=direction:anti-clockwise(1)/clockwise(2), third digit=duration:(2 seconds through 10 seconds ), fourth digit activate buzzer  
    msg_content = str(msg.payload).replace("b\'", "")
    msg_content = msg_content.replace("\'", "")
    msg_content = msg_content.split(",")
    print("Node:"+str(msg_content[0])+", Direction:"+str(msg_content[1])+", Duration:"+str(msg_content[2])+", Sound:"+str(msg_content[3]))
    
    if msg_content[0] == NODE_ID:
        print("Trigger detected")
        
        #if fourth digit = 1 turn on buzzer
        if msg_content[3] == "1":
            print("Turning on buzzer") 
            GPIO.output(RELAIS_3_GPIO, GPIO.HIGH)        # set GPIO to 1/GPIO.HIGH/True
            
        #if second digit  = 1 turn anti-clockwise
        if msg_content[1] == "1":
            print("Target about to face anti-clockwise") 
            GPIO.output(RELAIS_1_GPIO, GPIO.HIGH)        # set GPIO to 1/GPIO.HIGH/True  
            sleep(int(msg_content[2]))                   # wait for third digit duration  
    
            print("target about to edge")   
            GPIO.output(RELAIS_1_GPIO, GPIO.LOW)         # set GPIO to 0/GPIO.LOW/False
            sleep(0.5)
            #return motor to start
            GPIO.output(RELAIS_2_GPIO, GPIO.HIGH)
            print("target returning to edge...")
            #if park limit switch triggered stop motor
            GPIO.wait_for_edge(RELAIS_4_GPIO, GPIO.RISING)
            print("park limit switch reached")
            GPIO.output(RELAIS_2_GPIO, GPIO.LOW)

        #elseif second digit  = 2 turn clockwise
        elif msg_content[1] == "2":
            print("Target about to face clockwise") 
            GPIO.output(RELAIS_2_GPIO, GPIO.HIGH)        # set GPIO to 1/GPIO.HIGH/True  
            sleep(int(msg_content[2]))                   # wait for third digit duration  
            
            print("target about to edge")   
            GPIO.output(RELAIS_2_GPIO, GPIO.LOW)         # set GPIO to 0/GPIO.LOW/False
            sleep(0.5)
            #return motor to start
            GPIO.output(RELAIS_1_GPIO, GPIO.HIGH)
            print("target returning to edge...")
            #if park limit switch triggered stop motor
            GPIO.wait_for_edge(RELAIS_4_GPIO, GPIO.RISING)
            print("park limit switch reached")
            GPIO.output(RELAIS_1_GPIO, GPIO.LOW)

        if msg_content[3] == "1":
            print("Turning off buzzer") 
            GPIO.output(RELAIS_3_GPIO, GPIO.LOW)        # set GPIO to 0/GPIO.LOW/False

        print("target done")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(MQTTSERVER, int(MQTTPORT), 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
try:
    client.loop_forever()
except KeyboardInterrupt:          # trap a CTRL+C keyboard interrupt
    print("Quitting, gleaning up GPIO...")  
    GPIO.cleanup()                 # resets all GPIO ports used by this program  


